import java.util.*;

import static java.lang.Math.abs;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction lf1 = new Lfraction(3,5);
      System.out.println(lf1.toString());
      Lfraction lf2 = new Lfraction(4,7);
      System.out.println(lf1.plus(lf2));
      System.out.println(lf1.getCD(35,25));
      System.out.println(lf1.valueOf("-4/2/"));
   }

   private long numerator;
   private long denominator;

   public Lfraction () {
      //throw new RuntimeException (" Denominator and Numerator not given ");
      this(0, 1); // default
   }

   public Lfraction (long a) {
      //throw new RuntimeException (" Denominator is missing " + a);
      this(a, 1);
   }

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      long cd = getCD(abs(a), abs(b));
      if (b > 0) {
         numerator = a / cd;
         denominator = b / cd;
      } else if (b < 0) {
         numerator = -a / cd;
         denominator = -b / cd; // denominator must always be positive
      } else {
         throw new RuntimeException(" illegal denominator in: " + toString(a,b));
      }
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return toString(numerator, denominator);
   }
   private String toString(long a, long b) {
      return (a + "/" + b);
   }

   private static long getCD(long a, long b){ // Euclid's algorithm
      if (b == 0){
         return a;
      } else {
         return getCD(b, a%b);
      }
   }

   /* private Lfraction reduce(){
      long cd = getCD(numerator, denominator);
      return new Lfraction(numerator / cd, denominator / cd);
   } */

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      return (this.compareTo((Lfraction)m) == 0);
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return (int)(Double.doubleToLongBits(toDouble())>>31);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      return new Lfraction( m.numerator*denominator+numerator*m.denominator, m.denominator*denominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction(numerator*m.numerator, denominator*m.denominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if(numerator!=0) {
         return new Lfraction(denominator, numerator);
      } else {
         throw new RuntimeException(" illegal new denominator in inverse: " + toString());
      }
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-numerator, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return this.plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (numerator!=0) {
         return this.times(m.inverse());
      } else {
         throw new RuntimeException(" illegal new denominator in divideBy: " + toString());
      }
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long thislf = numerator*m.denominator;
      long mlf = m.numerator*denominator;
      return Long.compare(thislf, mlf);
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numerator,denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return numerator/denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction (numerator - denominator*integerPart(), denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)numerator/(double)denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      if (d > 0) {
         return new Lfraction((long)(Math.round(f*d)), d);
      } else {
         throw new RuntimeException(" denominator <= 0: "+ d);
      }
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] array = s.split("/");
      long count = s.chars().filter(ch -> ch == '/').count();
      if (count == 1) {
         try {
            long num = Long.parseLong(array[0]);
            long denom = Long.parseLong(array[1]);
            return new Lfraction(num, denom);
         } catch (Exception e){
            throw new RuntimeException(e + ": input elements are not of integer type: "+ s);
         }
      } else {
         throw new RuntimeException(" wrong input format: "+ s);
      }
   }
}

/* ALLIKAD:
https://www.java67.com/2012/08/java-program-to-find-gcd-of-two-numbers.html
https://mathbitsnotebook.com/JuniorMath/FractionsDecimals/FDcomparing.html
 */

